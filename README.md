# README #

Space/Off

Set in the year 317Z humanity has fled to the far reaches of space after invasion of their home planet of Earth. Their last hope, their prototype advanced turret. Attached to the final remaining colony ship, the UNS Charlie Chaplin, this turret is the last weapon between vicious and humanity. Your job, defend the ship, fire the turret, survive.


#Producer
Matthew Niederberger
#UI
Greg Carrobis
#Gameplay Programmer
Terrance Muchler
#Enemy
Chris Granville