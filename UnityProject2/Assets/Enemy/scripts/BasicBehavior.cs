﻿using UnityEngine;
using System.Collections;

public class BasicBehavior : MonoBehaviour {

    [SerializeField]
    private float maxspeed;
    private float maxforce = 1;
    private int maxRand = 100;

    private Vector3 velocity;
    private Vector3 acceleration = new Vector3(0,0,0);
    private Vector3 target;
    private bool attacking = false;
    private int rand;
    private GameObject player;

    [SerializeField]
    private float health = 1;

    [SerializeField]
    private GameObject destroyParticleSystem;


    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}

    // Update is called once per frame
    void FixedUpdate() {
        target = GameObject.FindGameObjectWithTag("target").transform.position;
        if (attacking == false)
            applyForce(Seek(target));
        rand = Random.Range(0, maxRand);
        //Debug.Log("rand: " + rand);
        if (rand == 50)
        {
            attacking = true;
        }
        if(attacking == true)
        {
            attack();
        }
        if (health <= 0)
        {
            Destroy(this.gameObject);

            if (destroyParticleSystem != null)
            {
                Instantiate(destroyParticleSystem, transform.position, Quaternion.identity);
            }
        }
    }
    Vector3 Seek(Vector3 target)
    {
        Vector3 desired = target - this.transform.position;
        desired.Normalize();
        desired *= maxspeed;

        Vector3 steer = desired - velocity;
        //steer.limit(maxforce);
        return steer;
    }
    void applyForce(Vector3 force)
    {
        acceleration += force;
        velocity += acceleration;
        this.transform.position += velocity;
        acceleration *= 0;
        velocity *= 0;
    }

    void attack()
    {
        //this.gameObject.AddComponent(Enemy);
        //Debug.Log("Attacking");
        applyForce(Seek(player.transform.position));
    }

    void OnCollisionEnter(Collision col)
    {
        MonoBehaviour script = col.gameObject.GetComponent<MonoBehaviour>();

        if (script is Bullet)
        {
            health -= (script as Bullet).damage;
        }
        if (col.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }
}
