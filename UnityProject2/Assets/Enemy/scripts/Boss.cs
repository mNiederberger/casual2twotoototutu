﻿using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour {
    private float angle = 0;
    private int rand;
    [SerializeField]
    private int health = 40;

    [SerializeField]
    private GameObject destroyParticleSystem;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        this.transform.Rotate(new Vector3(0, 0.05f, 0));
        if(this.transform.rotation.y >= 150 && this.transform.rotation.y >= 210)
        {
            attack();
        }
        //Debug.Log("boss health: " + health);
        if (health <= 0)
        {
            Destroy(this.gameObject);

            if (destroyParticleSystem != null)
            {
                Instantiate(destroyParticleSystem, transform.position, Quaternion.identity);
            }
        }
    }
    void attack()
    {
        rand = Random.Range(0, 100);
        Debug.Log("rand: " + rand);
        if (rand == 50)
        {
            GameObject targetPlayer = GameObject.FindGameObjectWithTag("player");
            GameObject ship = GameObject.Find("enemy attack");
            Vector3 lookVector = targetPlayer.transform.position - this.transform.position;
            //This line really got long...
            //Creates the enemies and gets a reference to their script
            Enemy e = ((GameObject)GameObject.Instantiate(ship, this.transform.position, Quaternion.Euler(lookVector))).GetComponent<Enemy>();
            e.targetPlayer = targetPlayer;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        MonoBehaviour script = col.gameObject.GetComponent<MonoBehaviour>();

        if (script is Bullet)
        {
            health -= (script as Bullet).damage;
        }
    }
}
