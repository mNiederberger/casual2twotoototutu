﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour {

    GameObject[] enemy;

    public bool waypoint; // else spawner

    private bool up = true;
    private bool swich = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (this.transform.position.y <= -40)
        {
            up = true;
        }
        if (this.transform.position.y >= 100)
        {
            up = false;
        }
        if (up == true) this.transform.position += new Vector3(0, 1, 0);
        if (up == false) this.transform.position += new Vector3(0, -1, 0);

    }

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("Collision");
        if (swich == true)
        { 
            this.transform.position = new Vector3(-32.2f, this.transform.position.y, this.transform.position.z);
            swich = false;
         }
        else if (swich == false)
        {
            this.transform.position = new Vector3(93.1f, this.transform.position.y, this.transform.position.z);
            swich = true;
        }
    }
}
