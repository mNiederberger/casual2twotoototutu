﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject spawnManager;

    [SerializeField]
    private Player player;

    [SerializeField]
    private GameObject pauseText;

    [SerializeField]
    private GameObject crosshair1;
    [SerializeField]
    private GameObject crosshair2;
    [SerializeField]
    private GameObject crosshair3;

    [SerializeField]
    private GameObject ammo1;
    [SerializeField]
    private GameObject ammo2;
    [SerializeField]
    private GameObject ammo3;


    // Use this for initialization
    void Start ()
    {
        crosshair1.SetActive(true);
        ammo1.SetActive(true);
	}
	
	// Update is called once per frame
	void Update ()
    {
        
    }
    
    public void TogglePause()
    {
        if(Time.timeScale <= 0)
        {
            Time.timeScale = 1;
            pauseText.SetActive(false);
        }
        else
        {
            Time.timeScale = 0;
            pauseText.SetActive(true);
        }


    }
    public void choose1()
    {
        Debug.Log("click1");
        player.equipAmmo = 0;
        chooseAmmo();
    }
    public void choose2()
    {
        Debug.Log("click2");
        player.equipAmmo = 1;
        chooseAmmo();
    }
    public void choose3()
    {
        Debug.Log("click3");
        player.equipAmmo = 2;
        chooseAmmo();
    }
    public void chooseAmmo()
    {
        crosshair1.SetActive(false);
        crosshair2.SetActive(false);
        crosshair3.SetActive(false);

        ammo1.SetActive(false);
        ammo2.SetActive(false);
        ammo3.SetActive(false);
        Debug.Log("Equip Ammo: " + player.equipAmmo);
        if (player.equipAmmo == 0)
        {
            crosshair1.SetActive(true);
            ammo1.SetActive(true);
        }
        else if (player.equipAmmo == 1)
        {
            crosshair2.SetActive(true);
            ammo2.SetActive(true);
        }
        else
        {
            crosshair3.SetActive(true);
            ammo3.SetActive(true);
            player.equipAmmo = 1;
        }
    }
}
