﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{

    public string type;
    public int damage;
    public float speed;


	[SerializeField]
	private GameObject hitParticleSystem;

    private float maxLifespan = 10f;
    private float startTime;

    // Use this for initialization
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > startTime + maxLifespan)
        {
            Destroy(this.gameObject);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        MonoBehaviour script = col.gameObject.GetComponent<MonoBehaviour>();

        if(script is Bullet)
        {
            Destroy(this.gameObject);
        }

		if(script is Enemy)
		{
			Destroy(this.gameObject);
			if (hitParticleSystem != null)
			{
				Instantiate(hitParticleSystem, this.transform.position, Quaternion.identity);
			}
		}
    }
}
