﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    //http://docs.unity3d.com/Manual/MobileInput.html
    private const float AccelerometerUpdateInterval = 1.0f / 60.0f;
    private const float LowPassKernelWidthInSeconds = .025f;

    private float pitchLowPassFilterFactor = AccelerometerUpdateInterval / LowPassKernelWidthInSeconds;
    private Vector3 pitchlowPassValue = Vector3.zero;

    private float compLowPassFilterFactor = AccelerometerUpdateInterval / LowPassKernelWidthInSeconds;
    private Vector3 complowPassValue = Vector3.zero;

    private float angle = 0f;
    private float pitch = 0f;

    private int health = 3;
    public float Angle
    {
        get { return angle; }
        set
        {
            angle = value;
            if (angle > 360)
            {
                angle -= 360;
            }
            if (angle < 0)
            {
                angle += 360;
            }
        }
    }

    public float Pitch
    {
        get
        {
            return pitch;
        }
        private set
        {
            pitch = value;
        }
    }

    [SerializeField]
    private float editorSpeedFactor;

    int[] ammunitionArray;

    [SerializeField]
    protected GameObject[] bulletPrebs;

    private int currentAmmoIndex;
    public int equipAmmo
    {
        get
        {
            return currentAmmoIndex;
        }
        set
        {
            currentAmmoIndex = value;
        }
    }

    private float speedAmmoCoolDown = 0.25f;
    private float powerAmmoCoolDown = 2.5f;
    private bool canFire = true;

    // Use this for initialization
    void Start()
    {
        pitchlowPassValue = Input.acceleration;
        complowPassValue = Input.compass.rawVector;
        currentAmmoIndex = 0;
        Input.compass.enabled = true;
        ammunitionArray = new int[2] { 15, 5 };
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_ANDROID

        //http://docs.unity3d.com/Manual/MobileInput.html
        float period = 0f;
        Vector3 acceleration = Vector3.zero;
        Vector3 magnometer = Input.compass.rawVector;
        
        for (int i = 0; i < Input.accelerationEventCount; i++)
        {
            acceleration += Input.accelerationEvents[i].acceleration;
            period = Input.accelerationEvents[i].deltaTime;
        }
        
        if (period > 0)
        {
            acceleration *= 1.0f / period;
        }

        acceleration.Normalize();
        magnometer.Normalize();

        //Apply a low pass filter and update the old lowPassValue and acceleration value
        acceleration = pitchlowPassValue = Vector3.Lerp(pitchlowPassValue, acceleration, pitchLowPassFilterFactor);
        magnometer = complowPassValue = Vector3.Lerp(complowPassValue, magnometer, compLowPassFilterFactor);

        Angle = Mathf.Atan2(acceleration.y, acceleration.x) * 180 / Mathf.PI + 90.0f;
        
        Pitch = acceleration.z * 90;

        Pitch = Mathf.Floor(Pitch);

        float Yaw = magnometer.y * 90;

        if (Yaw < -79)
            Yaw = -80;
        else if (Yaw > 79)
        {
            Yaw = 80;
        }
        Yaw = map(Yaw, -80, 80, -300, 300);
        //Yaw = Mathf.Floor(Yaw);
        
        

        //applies rotation
        //this.transform.rotation = Quaternion.Euler(-Pitch, Mathf.Floor(Input.compass.magneticHeading), 0);
        
        //Smoother but weird glitch where you rotate opposite direction after a while
        this.transform.rotation = Quaternion.Euler(-Pitch, Yaw, 0);

        this.transform.rotation *= Quaternion.AngleAxis(-Angle, Vector3.forward);
        //Debug.Log(acceleration);

#endif

#if UNITY_EDITOR
        //Angle += Input.GetAxisRaw("Horizontal") * editorSpeedFactor;
        //Debug.Log(Angle);
        //As it turns out, rotating a phone LITERALLY rotates your perspective. Who would have thought?
        this.transform.rotation *= Quaternion.AngleAxis(Input.GetAxisRaw("Horizontal") * editorSpeedFactor, Vector3.up);
        this.transform.rotation *= Quaternion.AngleAxis(-Input.GetAxisRaw("Vertical") * editorSpeedFactor, Vector3.right);
#endif

        //Firing Logic
        if ((Input.GetKeyDown(KeyCode.Space) || Input.touchCount > 0) && ammunitionArray[currentAmmoIndex] > 0)
        {
            StartCoroutine(fire());
        }


        Invoke("reload", 3.0f);

        if (Input.GetKeyDown(KeyCode.Q))
        {
            currentAmmoIndex += 1;
            if (currentAmmoIndex >= bulletPrebs.Length)
            {
                currentAmmoIndex = 0;
            }

            Debug.Log(currentAmmoIndex);
        }



    }

    float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    IEnumerator fire()
    {
        if (!canFire)
        {
            yield break;
        }
        canFire = false;
        Bullet bullet = ((GameObject)GameObject.Instantiate(bulletPrebs[currentAmmoIndex], transform.TransformDirection(Vector3.forward), Quaternion.Euler(transform.TransformDirection(Vector3.forward)))).GetComponent<Bullet>();
        Rigidbody brb = bullet.GetComponent<Rigidbody>();
        brb.velocity = bullet.speed * transform.TransformDirection(Vector3.forward);
        ammunitionArray[currentAmmoIndex] -= 1;
        Debug.Log("firing");
        Debug.Log(bullet.type + " has " + ammunitionArray[currentAmmoIndex]);
        if (currentAmmoIndex == 0)
            yield return new WaitForSeconds(speedAmmoCoolDown);
        else if (currentAmmoIndex == 1)
            yield return new WaitForSeconds(powerAmmoCoolDown);
        canFire = true;
    }

    void reload()
    {
        if (currentAmmoIndex == 0)
        {
            ammunitionArray[1] += 1;
            if (ammunitionArray[1] > 5)
                ammunitionArray[1] = 5;
        }
        else if (currentAmmoIndex == 1)
        {
            ammunitionArray[0] += 1;
            if (ammunitionArray[0] > 15)
                ammunitionArray[0] = 15;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        //MonoBehaviour script = col.gameObject.GetComponent<MonoBehaviour>();

        if (col.gameObject.tag == "enemy")
        {
            health -= 1;
        }
    }
}
