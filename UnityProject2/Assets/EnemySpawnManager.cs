﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawnManager : MonoBehaviour
{

	[SerializeField]
	private List<GameObject> enemyPrefabs = new List<GameObject>();

	[SerializeField]
	float spawnDist;

	[SerializeField]
	GameObject targetPlayer;

    private int spawnCount = 0;
    private int spawnLimit = 10;

    private float nextSpawnTime = 0;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        //Debug.Log("spawnCount" + spawnCount);
        if (Time.timeSinceLevelLoad > nextSpawnTime)
        {
            Vector3 dir;
            do
            {
                dir = Random.onUnitSphere;
            } while (dir.z < .4);
            //Debug.Log("spawnCount" + spawnCount);
            //if (spawnCount <= spawnLimit)
            //{
                //for (int i = 0; i < 7; i++) {
                    SpawnEnemy(dir);
                //}
                spawnCount++;
           // }
			nextSpawnTime = Time.timeSinceLevelLoad + Random.Range(3f, 4f);//3, 15
        }
	}
    
    void SpawnEnemy(Vector3 direction)
    {
		int index = Random.Range(0, enemyPrefabs.Count);
		Vector3 lookVector = targetPlayer.transform.position - (direction * spawnDist);
		//This line really got long...
		//Creates the enemies and gets a reference to their script
		Enemy e = ((GameObject) GameObject.Instantiate(enemyPrefabs[index], direction * spawnDist, Quaternion.Euler(lookVector))).GetComponent<Enemy>();
		//e.targetPlayer = targetPlayer;

        /*
        int index = Random.Range(0, enemyPrefabs.Count);
		Vector3 lookVector = targetPlayer.transform.position - (direction * spawnDist);
		//This line really got long...
		//Creates the enemies and gets a reference to their script
		GameObject e = ((GameObject) GameObject.Instantiate(enemyPrefabs[index], direction * spawnDist, Quaternion.Euler(lookVector)));
        e.AddComponent<BasicBehavior>();
        */
    }
}
