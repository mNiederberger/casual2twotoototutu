﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public GameObject targetPlayer { get; set; }

	private Rigidbody rb;

	[SerializeField]
	private float maxSpeed;

	[SerializeField]
	private float force;

    [SerializeField]
    private float health;

	[SerializeField]
	private GameObject destroyParticleSystem;

    private GameObject[] enemies;

	// Use this for initialization
	void Start ()
	{
		
	}

	// Update is called once per frame
	void Update()
	{
        enemies = GameObject.FindGameObjectsWithTag("enemy");
        foreach(GameObject gm in enemies)
        {
            Physics.IgnoreCollision(this.GetComponent<Collider>(), gm.GetComponent<Collider>(), true);
        }

		if (rb == null)
		{
			rb = this.GetComponent<Rigidbody>();
		}

		if (targetPlayer != null)
		{
            bool movingForward = Vector3.Dot(rb.velocity, (transform.position - targetPlayer.transform.position)) < 0;

            if (rb.velocity.magnitude < maxSpeed && movingForward) //Moving forward and not at max speed
			{
				Vector3 dir = targetPlayer.transform.position - transform.position;

				rb.AddForce(dir * force);

				//Debug.Log("Adding force " + force * dir);
			}
			else if(movingForward) //Moving forward but above max speed
			{
				rb.velocity = rb.velocity.normalized * maxSpeed;
			}
            else //Moving backwards
            {
                Vector3 dir = targetPlayer.transform.position - transform.position;

                rb.AddForce(dir * force);
            }
		}

        if(health <= 0)
        {
            Destroy(this.gameObject);

			if(destroyParticleSystem != null)
			{
				Instantiate(destroyParticleSystem, transform.position, Quaternion.identity);
			}
        }
	}

    void OnCollisionEnter(Collision col)
    {
        MonoBehaviour script = col.gameObject.GetComponent<MonoBehaviour>();

        if (script is Bullet)
        {
            health -= (script as Bullet).damage;
        }

        if(col.gameObject.tag == "player")
        {
            Destroy(this);
        }
    }
}
